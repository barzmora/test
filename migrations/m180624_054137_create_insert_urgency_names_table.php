<?php

use yii\db\Migration;

/**
 * Handles the creation of table `insert_urgency_names`.
 */
class m180624_054137_create_insert_urgency_names_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('urgency',array(
         'name'=>'low',
  ));
         $this->insert('urgency',array(
         'name'=>'normal',
  ));
        $this->insert('urgency',array(
         'name'=>'critical',
  ));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180624_054137_create_insert_urgency_names_table does not support migration down.\n";
  return false;
    }
}
