<?php

use yii\helpers\Html;
use yii\widgets\DetailView;


/* @var $this yii\web\View */
/* @var $model app\models\Task */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Tasks', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="task-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
    
         
            [                      
                'label' => 'urgency',
                'value' => $model->user->name,
            ],
            [
          'attribute' => 'created_at',
           'value' => date('Y-m-d H:i:s', $model->created_at)
       ],
             [
          'attribute' => 'updated_at',
          'value' => date('Y-m-d H:i:s', $model->updated_at)
       ],
      
         [
            'attribute'=>'created_by',
            'format'=>'raw',
            'value'=>Html::a($model->responsible->username, ['user/view', 'id' => $model->created_by]),
        ],
            [                      
                'label' => 'updated_by',
                'value' => $model->responsible2->username,
            ],
        ],
    ]) ?>

</div>
