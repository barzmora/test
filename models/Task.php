<?php

namespace app\models;
use yii\db\Expression;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;

use Yii;

/**
 * This is the model class for table "task".
 *
 * @property int $id
 * @property string $username
 * @property string $urgency
 * @property int $created_at
 * @property int $updated_at
 * @property int $created_by
 * @property int $updated_by
 */
class Task extends \yii\db\ActiveRecord
{
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
            BlameableBehavior::className(),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'task';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
         //   [['created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['name', 'urgency'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'name',
            'urgency' => 'Urgency',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }
    public function getResponsible(){
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }
     public function getResponsible2(){
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }
    public function getUser(){
        return $this->hasOne(Urgency::className(), ['id' => 'urgency']);
    }
}
