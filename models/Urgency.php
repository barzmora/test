<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "urgency".
 *
 * @property int $id
 * @property string $name
 */
class Urgency extends \yii\db\ActiveRecord
{

    public function getName()
    {
        return $this->name;
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'urgency';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }
}
